package cz.cvut.fel.swa.school.api;

import cz.cvut.fel.swa.school.UserService;
import cz.cvut.fel.swa.school.generated.controller.UsersApiDelegate;
import cz.cvut.fel.swa.school.generated.model.dto.CreateUserRequestDto;
import cz.cvut.fel.swa.school.generated.model.dto.UserDto;
import cz.cvut.fel.swa.school.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Log4j2
@RequiredArgsConstructor
public class InventoryApiImpl implements UsersApiDelegate {

  private final UserService userService;
  private final UserMapper userMapper;

  @Override
  public ResponseEntity<UserDto> createUser(CreateUserRequestDto createUserRequestDto) {
    return ResponseEntity.ok(
            userMapper.toDto(userService.createUser(createUserRequestDto)));
  }

  @Override
  public ResponseEntity<Void> deleteUser(String userId) {
    userService.deleteAccount(Long.parseLong(userId));
    return ResponseEntity.ok().build();
  }

  @Override
  public ResponseEntity<List<UserDto>> getAllUsers() {
    return ResponseEntity.ok(
            userService.getAllUsers().stream().map(userMapper::toDto).toList());
  }

  @Override
  public ResponseEntity<Void> enrollCourse(String userId, String courseId) {
    userService.enrollCourse(Long.parseLong(courseId), Long.parseLong(userId));
    return ResponseEntity.ok().build();
  }

  @Override
  public ResponseEntity<Void> leaveCourse(String userId, String courseId) {
    userService.leaveCourse(Long.parseLong(courseId), Long.parseLong(userId));
    return ResponseEntity.ok().build();
  }

  @Override
  public ResponseEntity<Void> borrowItem(String userId,
                                         String itemId,
                                         cz.cvut.fel.swa.school.generated.model.dto.BorrowItemRequestDto borrowItemRequestDto) {
    userService.borrowItem(Long.parseLong(userId), Long.parseLong(itemId), borrowItemRequestDto);
    return ResponseEntity.ok().build();
  }

  @Override
  public ResponseEntity<Void> returnItem(String userId, String itemId) {
    userService.returnItem(Long.parseLong(userId), Long.parseLong(itemId));
    return ResponseEntity.ok().build();
  }

  @Override
  public ResponseEntity<UserDto> getUserById(Integer userId) {
    return ResponseEntity.ok(userMapper.toDto(userService.getUser(userId.longValue())));
  }

  @Override
  public ResponseEntity<UserDto> getUserByUsername(String username) {
    return ResponseEntity.ok(userMapper.toDto(userService.getUser(username)));
  }

  @Override
  public ResponseEntity<Void> teachCourse(String userId, String courseId) {
    userService.teachCourse(Long.valueOf(courseId),Long.valueOf(userId));
    return ResponseEntity.ok().build();
  }
}
