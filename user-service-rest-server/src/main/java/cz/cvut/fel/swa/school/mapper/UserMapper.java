package cz.cvut.fel.swa.school.mapper;

import cz.cvut.fel.swa.school.domain.UserEntity;
import cz.cvut.fel.swa.school.generated.model.dto.UserDto;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {

  UserDto toDto(UserEntity item);
}
