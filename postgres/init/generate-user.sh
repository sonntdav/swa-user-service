#!/bin/bash
set -e

echo "SELECT 'CREATE DATABASE user_db' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'user_db')\gexec" | psql

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_NAME" <<-EOSQL
    CREATE USER my_user PASSWORD 'secret';
    GRANT ALL PRIVILEGES ON DATABASE user_db TO my_user;
EOSQL
