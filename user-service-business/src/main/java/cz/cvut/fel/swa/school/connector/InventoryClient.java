package cz.cvut.fel.swa.school.connector;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "inventory-service")
public interface InventoryClient {
    @PutMapping("/inventory/{itemId}/reservation")
    String borrowItemFromInventory(@PathVariable("itemId") String itemID, @RequestBody cz.cvut.fel.swa.school.generated.model.dto.BorrowItemRequestDto reservationRequestDto);

}
