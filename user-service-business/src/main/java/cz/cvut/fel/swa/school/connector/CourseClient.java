package cz.cvut.fel.swa.school.connector;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "course-service")
public interface CourseClient {
    @PutMapping("/parallel/{parallelId}/add-student/{studentId}")
    String addStudentToCourse(@PathVariable("parallelId") String parallelId, @PathVariable("studentId") String studentId,@RequestBody cz.cvut.fel.swa.school.generated.model.dto.AddStudentToParallelDto studentToParallelDto);

    @PutMapping("/parallel/{parallelId}/add-teacher/{teacherId}")
    String addTeacherToCourse(@PathVariable("parallelId") String parallelId, @PathVariable("teacherId") String studentId, @RequestBody cz.cvut.fel.swa.school.generated.model.dto.AddTeacherToParallelDto teacherToParallelDto);
}
