package cz.cvut.fel.swa.school;

import cz.cvut.fel.swa.school.domain.UserEntity;
import cz.cvut.fel.swa.school.generated.model.dto.CreateUserRequestDto;
import cz.cvut.fel.swa.school.generated.model.dto.UserDto;

import java.util.List;

public interface UserService {

    UserEntity getUser(Long id);

    UserEntity getUser(String username);

    List<UserEntity> getAllUsers();

    UserEntity createUser(CreateUserRequestDto userDTO);

    void deleteAccount(Long id);

    void enrollCourse(Long courseID, Long studentID);

    void leaveCourse(Long courseID, Long studentID);

    void teachCourse(Long courseID, Long teacherID);

    void stopTeachingCourse(Long courseID, Long teacherID);

    void borrowItem(Long teacherID, Long itemID, cz.cvut.fel.swa.school.generated.model.dto.BorrowItemRequestDto myDTO);

    void returnItem(Long teacherID, Long ItemID);
}
