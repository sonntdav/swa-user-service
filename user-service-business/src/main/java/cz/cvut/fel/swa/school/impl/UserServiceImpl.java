package cz.cvut.fel.swa.school.impl;

import com.google.common.collect.Lists;
import cz.cvut.fel.swa.school.UserService;
import cz.cvut.fel.swa.school.connector.CourseClient;
import cz.cvut.fel.swa.school.connector.InventoryClient;
import cz.cvut.fel.swa.school.domain.UserEntity;
import cz.cvut.fel.swa.school.domain.UserType;
import cz.cvut.fel.swa.school.generated.model.dto.AddStudentToParallelDto;
import cz.cvut.fel.swa.school.generated.model.dto.AddTeacherToParallelDto;
import cz.cvut.fel.swa.school.generated.model.dto.CreateUserRequestDto;
import cz.cvut.fel.swa.school.generated.model.dto.UserTypeDto;
import cz.cvut.fel.swa.school.repository.UserRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final InventoryClient inventoryClient;
    private final CourseClient courseClient;

    @Override
    public UserEntity getUser(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Account was not found. ID of account: " + id));
    }

    @Override
    public UserEntity getUser(String username) {
        return userRepository.findByUsername(username).orElseThrow(() -> new EntityNotFoundException("Account was not found. Username of account: " + username));
    }

    @Override
    public List<UserEntity> getAllUsers() {
        return Lists.newArrayList(userRepository.findAll());
    }

    @Override
    public UserEntity createUser(CreateUserRequestDto userDTO) {
        String originalUsername = userDTO.getUsername();

        // Check if a user with the same use rname already exists
        if (userRepository.findByUsername(originalUsername).isPresent()) {
            throw new EntityExistsException("Entity with this username already exists! Username: " + originalUsername);
        }
        UserEntity newUser = new UserEntity();

        newUser.setUsername(userDTO.getUsername());
        newUser.setFirstName(userDTO.getFirstName());
        newUser.setLastName(userDTO.getLastName());
        if (userDTO.getType() == UserTypeDto.STUDENT){
            newUser.setType(UserType.STUDENT);
        } else if (userDTO.getType() == UserTypeDto.TEACHER) {
            newUser.setType(UserType.TEACHER);
        }
        return userRepository.save(newUser);
    }


    @Override
    public void deleteAccount(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public void enrollCourse(Long courseID, Long studentID) {
        UserEntity user = findUserById(studentID);
        if (user.getType() != UserType.STUDENT){
            throw new EntityNotFoundException("User with this ID is not student, therefore he cant enroll a course! ID: " + studentID);
        }
        if (user.getEnrolledCourses().contains(courseID)){
            throw new EntityExistsException("Course with ID: " + courseID + " has already been enrolled for student with ID: " + studentID);
        }
        cz.cvut.fel.swa.school.generated.model.dto.AddStudentToParallelDto studentToParallelDto = new AddStudentToParallelDto();
        studentToParallelDto.setParallelId(courseID.toString());
        studentToParallelDto.setStudentId(studentID.toString());
        courseClient.addStudentToCourse(courseID.toString(),studentID.toString(),studentToParallelDto);
        user.getEnrolledCourses().add(courseID);
        userRepository.save(user);
    }

    @Override
    public void leaveCourse(Long courseID, Long studentID) {
        UserEntity user = findUserById(studentID);
        if (user.getType() != UserType.STUDENT){
            throw new EntityNotFoundException("User with this ID is not student, therefore he cant leave a course: " + studentID);
        }
        if (!user.getEnrolledCourses().contains(courseID)){
            throw new EntityNotFoundException("Course with ID: " + courseID + " has not been enrolled for student with ID: " + studentID);
        }
        user.getEnrolledCourses().remove(courseID);
        userRepository.save(user);
    }

    @Override
    public void teachCourse(Long courseID, Long teacherID) {
        UserEntity teacher = findUserById(teacherID);
        if (teacher.getType() != UserType.TEACHER){
            throw new EntityNotFoundException("User with this ID is not teacher, therefore he cant teach a course: " + teacher);
        }
        if (teacher.getTaughtCourses().contains(courseID)){
            throw new EntityExistsException("Course with ID: " + courseID + " is already taught by teacher with ID: " + teacherID);
        }
        cz.cvut.fel.swa.school.generated.model.dto.AddTeacherToParallelDto teacherToParallelDto = new AddTeacherToParallelDto();
        teacherToParallelDto.setParallelId(courseID.toString());
        teacherToParallelDto.setTeacherId(teacherID.toString());
        courseClient.addTeacherToCourse(courseID.toString(),teacherID.toString(),teacherToParallelDto);
        teacher.getTaughtCourses().add(courseID);
        userRepository.save(teacher);
    }

    @Override
    public void stopTeachingCourse(Long courseID, Long teacherID) {
        UserEntity teacher = findUserById(teacherID);
        if (teacher.getType() != UserType.TEACHER){
            throw new EntityNotFoundException("User with this ID is not teacher, therefore he cant teach a course: " + teacher);
        }
        if (!teacher.getTaughtCourses().contains(courseID)){
            throw new EntityExistsException("Course with ID: " + courseID + " is not taught by teacher with ID: " + teacherID);
        }
        teacher.getTaughtCourses().remove(courseID);
        userRepository.save(teacher);
    }

    @Override
    public void borrowItem(Long teacherID, Long itemID, cz.cvut.fel.swa.school.generated.model.dto.BorrowItemRequestDto myDTO) {
        UserEntity teacher = findUserById(teacherID);
        if (teacher.getType() != UserType.TEACHER){
            throw new EntityNotFoundException("User with this ID is not teacher, therefore he cant borrow an item: " + teacher);
        }
        if (teacher.getBorrowedItems().contains(itemID)){
            throw new EntityExistsException("Item with ID: " + itemID + " has already been borrowed by teacher with ID: " + teacherID);
        }
        teacher.getBorrowedItems().add(itemID);
        String result = inventoryClient.borrowItemFromInventory(itemID.toString(), myDTO);
        if (result != null){
            userRepository.save(teacher);
        }
    }


    @Override
    public void returnItem(Long teacherID, Long itemID) {
        UserEntity teacher = findUserById(teacherID);
        if (teacher.getType() != UserType.TEACHER){
            throw new EntityNotFoundException("User with this ID is not teacher, therefore he cant borrow an item: " + teacher);
        }
        if (!teacher.getBorrowedItems().contains(itemID)){
            throw new EntityExistsException("Item with ID: " + itemID + " has not been borrowed by teacher with ID: " + teacherID);
        }
        teacher.getBorrowedItems().remove(itemID);
        userRepository.save(teacher);
    }

    private UserEntity findUserById(Long id){
        return userRepository.findById(id).orElseThrow(() ->  new EntityNotFoundException("entity with this ID was not found! ID: " + id));
    }
}
