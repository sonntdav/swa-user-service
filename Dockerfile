FROM alpine:latest

# add open JDK 17
RUN apk update && apk upgrade && apk --update add openjdk17 tini bind-tools && rm -rf /tmp/* /var/cache/apk/*

ENV SERVER_PORT=8081

HEALTHCHECK	--interval=10s --timeout=3s --start-period=75s \
			CMD wget --quiet --tries=1 --spider http://localhost:$SERVER_PORT/actuator/health || exit 1

USER nobody:nobody
ARG JAR_FILE=user-service-boot/target/*.jar
COPY --chown=nobody:nobody ${JAR_FILE} app.jar
COPY --chown=nobody:nobody --chmod=755 ./entrypoint.sh /entrypoint.sh


ENTRYPOINT ["java","-jar","/app.jar"]

EXPOSE $SERVER_PORT
