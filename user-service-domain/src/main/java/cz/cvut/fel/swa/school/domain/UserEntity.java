package cz.cvut.fel.swa.school.domain;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Entity
@Data
@Table(name = "user_table")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String firstName;
    private String lastName;
    @Column(unique = true)
    private String username;
    private UserType type;
    @ElementCollection(fetch = FetchType.EAGER) // Changed the fetch type to EAGER
    @CollectionTable(name = "student_enrolled_courses")
    private List<Long> enrolledCourses;
    @ElementCollection(fetch = FetchType.EAGER) // Changed the fetch type to EAGER
    @CollectionTable(name = "teacher_taught_courses")
    private List<Long> taughtCourses;
    @ElementCollection(fetch = FetchType.EAGER) // Changed the fetch type to EAGER
    @CollectionTable(name = "borrowed_items")
    private List<Long> borrowedItems;
    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (!(o instanceof UserEntity)) return false;
      return id.equals(((UserEntity) o).id) && username.equals(((UserEntity) o).username);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", type=" + type +
                ", enrolledCourses=" + enrolledCourses +
                ", taughtCourses=" + taughtCourses +
                '}';
    }
}
