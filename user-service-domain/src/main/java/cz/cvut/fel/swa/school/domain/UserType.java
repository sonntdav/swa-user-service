package cz.cvut.fel.swa.school.domain;

public enum UserType {
    STUDENT,
    TEACHER
}
