package cz.cvut.fel.swa.school.repository;

import cz.cvut.fel.swa.school.domain.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

    Optional<UserEntity> findByUsername(String username);;
}
