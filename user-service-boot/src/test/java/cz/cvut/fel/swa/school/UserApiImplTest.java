package cz.cvut.fel.swa.school;

import static io.restassured.RestAssured.DEFAULT_PORT;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

import cz.cvut.fel.swa.school.UserService;
import cz.cvut.fel.swa.school.UserServiceApplication;
import cz.cvut.fel.swa.school.generated.model.dto.CreateUserRequestDto;
import cz.cvut.fel.swa.school.generated.model.dto.UserTypeDto;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.swagger.v3.core.util.Json;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@ContextConfiguration(classes = UserServiceApplication.class)
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Testcontainers
class InventoryApiImplTest {

    @Container
    private static final PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>(
            "postgres:latest")
            .withDatabaseName("integration-tests-db").withUsername("username").withPassword("password");

    static {
        postgreSQLContainer.start();
    }

    @Autowired
    private UserService userService;

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
        System.setProperty("spring.datasource.driver-class-name",
                postgreSQLContainer.getDriverClassName());
        dynamicPropertyRegistry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        dynamicPropertyRegistry.add("spring.datasource.username", postgreSQLContainer::getUsername);
        dynamicPropertyRegistry.add("spring.datasource.password", postgreSQLContainer::getPassword);
    }

    @BeforeAll
    public static void setUp() {
        RestAssured.port = DEFAULT_PORT;
    }

    @Test
    void createStudent() {
        cz.cvut.fel.swa.school.generated.model.dto.CreateUserRequestDto dto = new CreateUserRequestDto(
                "firstName",
                "lastName",
                "username",
                UserTypeDto.STUDENT
        );

        var item = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(Json.pretty(dto))
                .put("/inventory")
                .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .as(cz.cvut.fel.swa.school.generated.model.dto.UserDto.class);

        Assert.assertEquals(dto.getFirstName(), item.getFirstName());
        Assert.assertNotNull(item.getId());
        Assert.assertEquals(dto.getType(), item.getType());
        Assert.assertEquals(dto.getUsername(), item.getUsername());
        Assert.assertEquals(dto.getLastName(), item.getLastName());
    }

    @Test
    void createTeacher() {
        cz.cvut.fel.swa.school.generated.model.dto.CreateUserRequestDto dto = new CreateUserRequestDto(
                "firstName",
                "lastName",
                "username",
                UserTypeDto.TEACHER
        );

        var item = RestAssured.given()
                .contentType(ContentType.JSON)
                .body(Json.pretty(dto))
                .put("/inventory")
                .then()
                .assertThat()
                .statusCode(200)
                .extract()
                .as(cz.cvut.fel.swa.school.generated.model.dto.UserDto.class);

        Assert.assertEquals(dto.getFirstName(), item.getFirstName());
        Assert.assertNotNull(item.getId());
        Assert.assertEquals(dto.getType(), item.getType());
        Assert.assertEquals(dto.getUsername(), item.getUsername());
        Assert.assertEquals(dto.getLastName(), item.getLastName());
    }

    @Test
    void deleteUser() {
        cz.cvut.fel.swa.school.generated.model.dto.CreateUserRequestDto dto = new CreateUserRequestDto(
                "firstName",
                "lastName",
                "username",
                UserTypeDto.TEACHER
        );
        var rev = userService.createUser(dto);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .pathParam("userId", rev.getId())
                .delete("/users/{userId}")
                .then()
                .assertThat()
                .statusCode(200);

        var list = userService.getUser(dto.getUsername());
        Assert.assertNull(list);
    }


}